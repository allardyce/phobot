# PhoBot

A command line utility to compress, resize and manipulate images for web design.

![Screnshot of Phobot in Action](./public/screenshot.png)

## Commands

### Input and Output Overview

`phobot <action> [input] [output]`

By default, the `input` is all of the images in the current working directory. If no `output` is specified, all images will be overwritten.

`phobot <action> hello.jpg`

Modify a single image, overwrites image

`phobot <action> hello.jpg hello.modified.jpg`

Modify single image, outputs under different filename

`phobot <action> .`

Modify all images in current working directory

`phobot <action> . ./modified`

Modify all images in current working directory and output to modified folder

## Setup

1. Clone respository `git clone https://gitlab.com/allardyce/phobot.git`

2. Install dependancies `yarn install`

3. Link to path `yarn link`

4. Access from any command line
