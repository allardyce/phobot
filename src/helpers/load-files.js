const fs = require('fs')
const path = require('path')

const log = require('./log');


//Walk directory recursively to find all files
const walkSync = (dir, filelist = []) => {
    fs.readdirSync(dir).forEach(file => {  
      filelist = fs.statSync(path.join(dir, file)).isDirectory()
        ? walkSync(path.join(dir, file), filelist)
        : filelist.concat(path.join(dir, file));  
    });
    return filelist;
}



module.exports = (paths) => {

    if (!Array.isArray(paths)) paths = [paths];

    log.verbose('Searching for files. Criteria:', paths);

    let output = [];

    paths.forEach(path => {
        if (fs.existsSync(path)) {
            if (fs.statSync(path).isDirectory()) {
                output = output.concat(walkSync(path));
            } else {
                output.push(path);
            }
        }
    });
    
    log.verbose(`Files found:`, `(${output.length})`, output);

    return output;

}