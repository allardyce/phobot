const fs = require('fs');
const path = require('path');

const log = require('./log');

//Walk directory recursively to find all files
const walkSync = (dir, filelist = []) => {
    fs.readdirSync(dir).forEach(file => {
        filelist = fs.statSync(path.join(dir, file)).isDirectory() ? walkSync(path.join(dir, file), filelist) : filelist.concat(path.join(dir, file));
    });
    return filelist;
};

const IMAGE_EXTENSIONS = ['.jpeg', '.jpg', '.png'];

module.exports = async (input, output, recursive, filter_extensions = true) => {
    //Fix arugments if output not supplied
    if (!input) {
        input = '.';
    }
    if (!output) {
        output = '.';
    }

    input = input.split(',');

    if (input.some(path => !fs.existsSync(path))) {
        log.error('Input file/folder does not exist');
        process.exit(1);
    }

    //Sort through all inputs
    let inputImages = input.reduce((images, file) => {
        if (fs.statSync(file).isDirectory()) {
            let files;

            if (recursive) {
                files = walkSync(file);
            } else {
                files = fs.readdirSync(file).map(f => path.join(file, f));
            }

            if (filter_extensions) {
                files = files.filter(file => IMAGE_EXTENSIONS.includes(path.parse(file).ext.toLowerCase()));
            } else {
                // Only files, no directories
                files = files.filter(file => path.parse(file).ext.toLowerCase());
            }

            images = images.concat(files);
        } else {
            images.push(file);
        }

        return images;
    }, []);

    //normalize all found files
    inputImages = inputImages.map(image => path.normalize(image));

    //Remove duplicates
    inputImages = [...new Set(inputImages)];

    //Output
    output = path.normalize(output);

    //Meh. Check if output is directory by looking for extension. Directories can have . though...
    let outputIsDir = !path.parse(output).ext;

    if (outputIsDir && !fs.existsSync(output)) {
        log(`Creating directory: ${output}.`);
        fs.mkdirSync(output, { recursive: true });
    }

    if (inputImages.length > 1 && !outputIsDir) {
        log.error('Multiple input images must output to a directory');
        process.exit(1);
    }

    let files = inputImages.map(image => ({
        input: image,
        output: outputIsDir ? path.join(output, image) : output
    }));

    log.verbose(`Files found:`, `(${files.length})`, output);

    return files;
};
