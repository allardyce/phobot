const chalk = require('chalk');

let verbose = false;

const log = (...message) => {

    console.log(...message);

}

log.verbose = (...message) => {

    if (!message.length) {
        log('Enabling verbose output.');
        verbose = true;
        return;
    }

    if (verbose) {
        console.log(...message);
    }

}

log.error = (...message) => {

    console.log(chalk.red(...message));

    process.exit(1);

}

module.exports = log;