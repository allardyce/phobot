const fs = require('fs');

module.exports = (filepath) => {

    let descriptor = fs.openSync(filepath, 'r');
    
    let stats = fs.fstatSync(descriptor);

    let size = stats.size;

    if (size <= 0) {
        return new Error('File size is not greater than 0 —— ' + filepath);
    }

    let bufferSize = Math.min(size, 65535);

    let buffer = Buffer.alloc(bufferSize);    
    
    fs.readSync(descriptor, buffer, 0, bufferSize, 0);

    fs.closeSync(descriptor);

    return buffer;

}