var { promisify } = require('util');
const getPixels = promisify(require('get-pixels'));

const componentToHex = c => {
    var hex = c.toString(16);
    return hex.length == 1 ? '0' + hex : hex;
};
const rgbToHex = rgb => {
    return '#' + componentToHex(rgb.r) + componentToHex(rgb.g) + componentToHex(rgb.b);
};

module.exports = async path => {
    let pixels = await getPixels(path);

    let length = pixels.data.length,
        i = -4,
        rgb = { r: 0, g: 0, b: 0 },
        count = 0;

    while ((i += 4 * 4) < length) {
        ++count;
        rgb.r += pixels.data[i];
        rgb.g += pixels.data[i + 1];
        rgb.b += pixels.data[i + 2];
    }

    // ~~ used to floor values
    rgb.r = ~~(rgb.r / count);
    rgb.g = ~~(rgb.g / count);
    rgb.b = ~~(rgb.b / count);

    let average = rgbToHex(rgb);

    return average;
};
