#! /usr/bin/env node

const phobot = require('commander');
const { version } = require('../package');

const log = require('./helpers/log');

const resize = require('./commands/resize');
const svg = require('./commands/svg');
const vue = require('./commands/vue');
const compress = require('./commands/compress');
const webp = require('./commands/webp');
const web = require('./commands/web');
const placeholder = require('./commands/placeholder');
const extension = require('./commands/extension');

process.on('exit', () => {
    log('Script finished in', process.uptime(), 'seconds.');
});

phobot.version(version);

phobot
    .command('compress [input] [output]')
    .description('compress images')
    .option('-q,--quality,[quality]', 'quality of compression', parseInt)
    .option('-r,--recursive,[recursive]', 'recurse into subdirectories')
    .action(compress);

phobot
    .command('webp [input] [output]')
    .description('convert images to webp')
    .option('-q,--quality,[quality]', 'quality of compression for jpg', parseInt)
    .option('-r,--recursive,[recursive]', 'recurse into subdirectories')
    .action(webp);

phobot
    .command('web [input] [output]')
    .description('convert images to web friendly versions')
    .option('-q,--quality,[quality]', 'quality of compression for jpg', parseInt)
    .option('-s,--size,[size]', 'quality of compression for jpg', parseInt)
    .option('-r,--recursive,[recursive]', 'recurse into subdirectories')
    .action(web);

phobot
    .command('resize [input] [output]')
    .description('Resize an image to fit the dimensions')
    .option('-r,--recursive,[recursive]', 'recurse into subdirectories')
    .option('-w,--width,[width]', 'width of the photo', parseInt)
    .option('-h,--height,[height]', 'height of the photo', parseInt)
    .option('-f,--fit,[fit]', 'fit for resize')
    .option('-e,--withoutEnlargement,[withoutEnlargement]', 'do not enlarge if already smaller')
    .action(resize);

phobot
    .command('vue [input] [output]')
    .description('resize images for vue photoview')
    .option('-r,--recursive,[recursive]', 'recurse into subdirectories')
    .option('-p,--prefix,[prefix]', 'prefix path for meta data')
    .option('-c,--clean,[clean]', 'clean up old files that no longer exist')
    .option('-v,--verbose,[verbose]', 'verbose output')
    .action(vue);

phobot
    .command('svg [input] [output]')
    .description('Convert a photo to svg')
    .option('-r,--recursive,[recursive]', 'recurse into subdirectories')
    .option('-s,--steps,[steps]', 'number of steps in the SVG', parseInt)
    .option('-m,--mutations,[mutations]', 'number of mutations to try per step', parseInt)
    .action(svg);

phobot
    .command('placeholder [input] [output]')
    .description('Generate a placeholder image')
    .option('-r,--recursive,[recursive]', 'recurse into subdirectories')
    .option('-w,--width,[width]', 'width of the photo', parseInt)
    .option('-h,--height,[height]', 'height of the photo', parseInt)
    .action(placeholder);

phobot
    .command('extension [input] [output]')
    .description('Folderise files by extension')
    .option('-r,--recursive,[recursive]', 'recurse into subdirectories')
    .action(extension);

phobot.command('*').action(() => phobot.help());

phobot.parse(process.argv);

// if (!process.argv.slice(2).length) {
//     phobot.help();
// }
