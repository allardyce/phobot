const fs = require('fs');
const path = require('path');
var { promisify } = require('util');
const sizeOf = promisify(require('image-size'));
const sharp = require('sharp');
const exifParser = require('exif-parser');
const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');

const log = require('../helpers/log');
const formatBytes = require('../helpers/format-bytes');
const parseInputOutput = require('../helpers/parse-input-output');
const fileToBuffer = require('../helpers/file-to-buffer');
const averageColour = require('../helpers/average-colour');

//Settings
let settings = {
    sizes: [384, 1024, 2048],
    prefix_path: '/'
};

// original -> webpath
// original -> resize
// resize -> original

const originalToWebpath = (dir, output) => {
    let webPath = dir.split(path.sep);

    let hops = output.split(path.sep).length;

    for (let i = 0; i < hops; i++) {
        webPath.shift();
    }

    return path.normalize(settings.prefix_path + webPath.join(path.sep));
};

const Meta = {
    read(dir) {
        let meta = [];

        if (fs.existsSync(path.join(dir, 'meta.js'))) {
            meta = eval(fs.readFileSync(path.normalize(path.join(dir, 'meta.js'))).toString());
        }

        return meta;
    },

    write(dir, data) {
        let output = 'module.exports = [';

        data.forEach(item => {
            output += `
{
    large: { src: '${item.large.src}', w: ${item.large.w}, h: ${item.large.h} },
    medium: { src: '${item.medium.src}', w: ${item.medium.w}, h: ${item.medium.h} },
    msrc: '${item.msrc}',`;

            if (item.date) {
                output += `
    date: ${item.date},`;
            }

            if (item.title) {
                output += `
    title: '${item.title}',`;
            }

            if (item.background) {
                output += `
    background: '${item.background}',`;
            }

            if (item.gps && item.gps.lat) {
                output += `
    gps: { lat: ${item.gps.lat}, lng: ${item.gps.lng} }`;
            }

            output += `
},`;
        });

        output += `
]`;

        fs.writeFileSync(path.join(dir, 'meta.js'), output);
    }
};

//Find all images that need resizing
const findResizes = files => {
    let resizes = [];

    files.forEach(original => {
        settings.sizes.forEach(size => {
            let parse = path.parse(original.output);

            let resize = `${path.join(parse.dir, parse.name)}.${size}${parse.ext.toLowerCase()}`;

            if (!fs.existsSync(resize) || settings.overwrite) {
                log.verbose(`Compress and resize ${original.input} to ${size}px, output to ${resize}`);

                resizes.push({
                    input: original.input,
                    output: resize,
                    size: size
                });
            }
        });
    });

    return resizes;
};

const getResizes = resizes => {
    let resizing = [];

    resizes.forEach(resize => {
        let parse = path.parse(resize.output);

        if (!fs.existsSync(parse.dir) && parse.dir) fs.mkdirSync(parse.dir, { recursive: true });

        resizing.push(
            new Promise((resolve, reject) => {
                log.verbose(`Resizing ${resize.input} to ${resize.size}px`);

                resolve(
                    sharp(resize.input, { failOnError: false })
                        .rotate()
                        .resize({
                            width: resize.size,
                            height: resize.size,
                            fit: 'inside'
                        })
                        .jpeg()
                        .toBuffer()
                        .then(async resize => {
                            try {
                                return await imagemin.buffer(resize, {
                                    use: [
                                        imageminMozjpeg({
                                            quality: 75
                                        })
                                    ]
                                });
                            } catch (error) {
                                console.error(error);
                                log.error(`[Imagemin] Error compressing ${resize.input}`);
                            }
                        })
                        .then(image => {
                            let writeStream = fs.createWriteStream(resize.output);

                            writeStream.write(image);

                            writeStream.on('finish', () => {
                                log(`Resized ${resize.input} -> ${resize.output}`);
                            });

                            writeStream.end();

                            return image;
                        })
                        .catch(error => {
                            log.error(`Error processing ${resize.input}`);
                        })
                );
            })
        );
    });

    return resizing;
};

const generateMeta = async (files, output) => {
    for (let file of files) {
        let image = path.parse(file.output);

        let webPath = originalToWebpath(image.dir, output);

        let meta = Meta.read(image.dir);

        if (!meta.find(item => item.large && item.large.src === path.join(webPath, `${image.name}.2048${image.ext}`).replace(/\\/g, '/'))) {
            //Find image dimensions
            let [large_dimensions, medium_dimensions] = await Promise.all([await sizeOf(path.normalize(path.join(image.dir, image.name + '.2048' + image.ext))), await sizeOf(path.normalize(path.join(image.dir, image.name + '.1024' + image.ext)))]);

            let photo = {
                large: { src: path.join(webPath, `${image.name}.2048${image.ext}`), w: large_dimensions.width, h: large_dimensions.height },
                medium: { src: path.join(webPath, `${image.name}.1024${image.ext}`), w: medium_dimensions.width, h: medium_dimensions.height },
                msrc: path.join(webPath, `${image.name}.384${image.ext}`),
                title: ''
            };

            //Fix windows path to web path \ -> /
            photo.large.src = photo.large.src.replace(/\\/g, '/');
            photo.medium.src = photo.medium.src.replace(/\\/g, '/');
            photo.msrc = photo.msrc.replace(/\\/g, '/');

            let thumbnail = path.normalize(path.join(image.dir, image.name + '.384' + image.ext));

            photo.background = await averageColour(thumbnail);

            //Get GPS Coords
            let buffer = fileToBuffer(file.input),
                parse;
            try {
                parse = exifParser.create(buffer).parse();
            } catch (error) {
                console.log(`Failed to exif parse: ${file.input}`);
            }

            if (parse && parse.tags.GPSLatitude) {
                photo.gps = {};
                photo.gps.lat = parse.tags.GPSLatitude;
                photo.gps.lng = parse.tags.GPSLongitude;
            }

            if (parse && parse.tags.DateTimeOriginal) {
                photo.date = parseInt(parse.tags.DateTimeOriginal);
            }

            meta.push(photo);

            meta.sort((a, b) => a.date - b.date);

            Meta.write(image.dir, meta);
        }
    }
};

const deleteMissing = async output => {
    let counter = 0;

    //Search output directory for all files
    let files = await parseInputOutput(output, '.', true);

    for (let file of files) {
        let parse = path.parse(file.input);

        //Resized dir to original dir
        let originalDir = parse.dir.split(path.sep);
        let hops = output.split(path.sep).length;
        for (let i = 0; i < hops; i++) {
            originalDir.shift();
        }
        originalDir = originalDir.join(path.sep);

        //Resized name to original name
        let originalName = parse.name.split('.');
        originalName.pop();
        originalName = originalName.join('.');

        //Resized filepath to original filepath
        let original = `${path.join(originalDir, originalName)}${parse.ext}`;

        //Original file no longer exists
        if (!fs.existsSync(original)) {
            //Remove file
            fs.unlinkSync(file.input);

            //Remove image details from meta data
            let webPath = originalToWebpath(parse.dir, output);

            let meta = Meta.read(parse.dir);

            let index = meta.findIndex(item => item.large && item.large.src === path.join(webPath, `${originalName}.2048${parse.ext}`).replace(/\\/g, '/'));

            if (index > -1) {
                meta.splice(index, 1);
                Meta.write(parse.dir, meta);
            }

            log(`Deleted ${original}`);

            counter++;
        }
    }

    return counter;
};

const gogogo = async (input, output = '', { recursive, prefix, clean, verbose }) => {
    if (verbose) log.verbose();

    if (prefix) settings.prefix_path = prefix;

    output = path.normalize(output);

    log('Searching for images..');

    let files = await parseInputOutput(input, output, recursive);

    log(`Found ${files.length} images.`);

    log('Searching for resizes..');

    let resizes = findResizes(files);

    log(`Found ${resizes.length} images to process.`);

    log('Processing..');

    let resized = await Promise.all(getResizes(resizes));

    log(`Resized ${resized.length} images.`);

    log('Generating meta for resized images');

    await generateMeta(files, output);

    log('Meta data generated.');

    if (clean) {
        log('Cleaning missing images');

        let deleted = await deleteMissing(output);

        log(`Removed ${deleted} images`);
    }

    log('Script complete.');
};

module.exports = gogogo;
