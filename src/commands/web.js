const fs = require('fs');
const { promisify } = require('util');
const readFile = promisify(fs.readFile);
const sharp = require('sharp');
const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminOptipng = require('imagemin-optipng');
const imageminWebp = require('imagemin-webp');

const parseInputOutput = require('../helpers/parse-input-output');
const formatBytes = require('../helpers/format-bytes');
const log = require('../helpers/log');


const compressImageToWebp = async (imageBuffer, options) => {
    return await imagemin.buffer(imageBuffer, {
        use: [
            imageminWebp({
                preset: 'photo',
                quality: options.quality - 10
            })
        ]
    });
}
const compressImage = async (imageBuffer, options) => {
    return await imagemin.buffer(imageBuffer, {
        use: [
            imageminMozjpeg({
                quality: options.quality
            }),
            imageminOptipng()
        ]
    });
}


const save = (file, buffer) => {
    let writeStream = fs.createWriteStream(file);
    writeStream.write(buffer);
    writeStream.on('finish', () => {
        log(`Saved ${file} (${formatBytes(buffer.length)})`);
    });
    writeStream.end();
}

const web = async (input, output, { recursive, quality = 75, size = 2048 }) => {

    let files = await parseInputOutput(input, output, recursive);

    log(`Found ${files.length} files. Starting compression algorithms. Beep boop.`);

    files.forEach(async file => {
        
        let original = await readFile(file.input);

        console.log(`Resizing ${file.input} to ${size}px.`);
        
        let image = await sharp(original, { failOnError: false })
            .resize({ width: size, height: size, fit: 'inside' })
            .toBuffer();

        console.log(`Resized. Now compressing.`);

        let [ imageCompressed, imageWebp ] = await Promise.all([
                await compressImage(image, { quality }),
                await compressImageToWebp(image, { quality }),
            ]);

        let output = file.output.split('.');
        let ext = output.pop();
        output = output.join('.');
        
        console.log(`Original file: ${file.input} (${formatBytes(original.length)})`);

        save(`${output}.${ext}`, imageCompressed);
        save(`${output}.webp`, imageWebp);

    });

}

module.exports = web;