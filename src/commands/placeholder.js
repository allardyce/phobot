var { promisify } = require('util');
const fs = require('fs');
const path = require('path');
const sharp = require('sharp');
const svgDataUri = require('mini-svg-data-uri');
const sizeOf = require('image-size');

const log = require('../helpers/log');
const formatBytes = require('../helpers/format-bytes');
const parseInputOutput = require('../helpers/parse-input-output');

const bufferToBlurredSVG = (buffer, width, height) => {
    let id = Math.random()
        .toString(36)
        .substr(2, 10);
    return svgDataUri(`<svg fill="none" viewBox="0 0 ${width} ${height}" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><filter id="${id}">
        <feGaussianBlur in="SourceGraphic" stdDeviation="40"/></filter></defs><image x="0" y="0" filter="url(#${id})" width="${width}" height="${height}" xlink:href="data:image/jpeg;base64,${buffer.toString('base64')}" /></svg>`);
};

const resize = async (input, output, { recursive, width, height }) => {
    if (!width && !height) {
        width = 64;
    }

    let files = await parseInputOutput(input, output, recursive);

    log(`Found ${files.length} files. Starting resize algorithms. Beep boop.`);

    files.forEach(file => {
        let fileStats = fs.statSync(file.input);
        const dimensions = sizeOf(file.input);
        console.log(dimensions);

        sharp(file.input, { failOnError: false })
            .jpeg()
            .resize({
                width: width,
                height: height,
                fit: 'inside'
            })
            .toBuffer({ resolveWithObject: true })
            .then(({ data, info }) => {
                let parse = path.parse(file.output);
                let output = path.join(parse.dir, `${parse.name}.placeholder`);

                data = bufferToBlurredSVG(data, dimensions.width, dimensions.height);

                let writeStream = fs.createWriteStream(output);

                writeStream.write(data);

                writeStream.on('finish', () => {
                    log(`Resized ${file.input} (${formatBytes(fileStats.size)}) -> ${output} (${formatBytes(data.length)}) - (${((data.length / fileStats.size) * 100).toFixed(2)}%)`);
                });

                writeStream.end();
            });
    });
};

module.exports = resize;
