const fs = require('fs');
const { promisify } = require('util');
const readFile = promisify(fs.readFile);
const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminOptipng = require('imagemin-optipng');
const imageminjpegrecompress = require('imagemin-jpeg-recompress');

const parseInputOutput = require('../helpers/parse-input-output');
const formatBytes = require('../helpers/format-bytes');
const log = require('../helpers/log');

const compressImage = async (imageBuffer, options) => {
    return await imagemin.buffer(imageBuffer, {
        plugins: [
            imageminjpegrecompress({
                quality: options.quality || 75
            }),
            imageminOptipng()
        ]
    });
};

const compress = async (input, output, { recursive, quality }) => {
    let files = await parseInputOutput(input, output, recursive);

    log(`Found ${files.length} files. Starting compression algorithms. Beep boop.`);

    files.forEach(async file => {
        let original = await readFile(file.input);

        let compressed;
        try {
            compressed = await compressImage(original, { quality });
        } catch (e) {
            console.log(e);
            process.exit(1);
        }

        if (compressed.length > original.length) {
            log(`Compression increased filesize, not saving. ${file.input} (${formatBytes(original.length)}) -> ${file.output} (${formatBytes(compressed.length)}) - (${((compressed.length / original.length) * 100).toFixed(2)}%)`);
            return;
        }

        let writeStream = fs.createWriteStream(file.output);

        writeStream.write(compressed);

        writeStream.on('finish', () => {
            log(`Saved ${file.input} (${formatBytes(original.length)}) -> ${file.output} (${formatBytes(compressed.length)}) - (${((compressed.length / original.length) * 100).toFixed(2)}%)`);
        });

        writeStream.end();
    });
};

module.exports = compress;
