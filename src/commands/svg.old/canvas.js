const util = require('./util.js')
const Image = require('canvas-prebuilt').Image


const jsdom = require('jsdom')
const { JSDOM } = jsdom
const { document } = (new JSDOM(`<!DOCTYPE html><html><body></body></html>`)).window


function getScale(width, height, limit) {
	return Math.max(width / limit, height / limit, 1);
}

/* FIXME move to util */
function getFill(canvas) {
	let data = canvas.getImageData();
	let w = data.width;
	let h = data.height;
	let d = data.data;
	let rgb = [0, 0, 0];
	let count = 0;
	let i;

	for (let x=0; x<w; x++) {
		for (let y=0; y<h; y++) {
			if (x > 0 && y > 0 && x < w-1 && y < h-1) { continue; }
			count++;
			i = 4*(x + y*w);
			rgb[0] += d[i];
			rgb[1] += d[i+1];
			rgb[2] += d[i+2];
		}
	}

	rgb = rgb.map(x => ~~(x/count)).map(util.clampColor);
	return `rgb(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`;
}

function svgRect(w, h) {
	let node = document.createElementNS("http://www.w3.org/2000/svg", "rect");
	node.setAttribute("x", 0);
	node.setAttribute("y", 0);
	node.setAttribute("width", w);
	node.setAttribute("height", h);

	return node;
}

/* Canvas: a wrapper around a <canvas> element */
class Canvas {
	static empty(cfg, svg) {
		if (svg) {
			let node = document.createElementNS("http://www.w3.org/2000/svg", "svg");
			node.setAttribute("viewBox", `0 0 ${cfg.width} ${cfg.height}`);
			node.setAttribute("clip-path", "url(#clip)");

			let defs = document.createElementNS("http://www.w3.org/2000/svg", "defs");
			node.appendChild(defs);

			let cp = document.createElementNS("http://www.w3.org/2000/svg", "clipPath");
			defs.appendChild(cp);
			cp.setAttribute("id", "clip");
			cp.setAttribute("clipPathUnits", "objectBoundingBox");
			
			let rect = svgRect(cfg.width, cfg.height);
			cp.appendChild(rect);

			rect = svgRect(cfg.width, cfg.height);
			rect.setAttribute("fill", cfg.fill);
			node.appendChild(rect);

			return node;
		} else {
			return new this(cfg.width, cfg.height).fill(cfg.fill);
		}
	}

	static original(image, config) {

		return new Promise(resolve => {

			let img = new Image();
			img.src = image

			let w = img.width;
			let h = img.height;

			let computeScale = getScale(w, h, config.computeSize);
			config.width = w / computeScale;
			config.height = h / computeScale;

			let viewScale = getScale(w, h, config.viewSize);

			config.scale = computeScale / viewScale;

			let canvas = this.empty(config);
			canvas.ctx.drawImage(img, 0, 0, config.width, config.height);

			if (config.fill == "auto") { config.fill = getFill(canvas); }

			resolve(canvas);
		});
	}

	constructor(width, height) {
		this.node = document.createElement("canvas");
		this.node.width = width;
		this.node.height = height;
		this.ctx = this.node.getContext("2d");
		this._imageData = null;
	}

	clone() {
		let otherCanvas = new this.constructor(this.node.width, this.node.height);
		otherCanvas.ctx.drawImage(this.node, 0, 0);
		return otherCanvas;
	}

	fill(color) {
		this.ctx.fillStyle = color;
		this.ctx.fillRect(0, 0, this.node.width, this.node.height);
		return this;
	}

	getImageData() {
		if (!this._imageData) {
			this._imageData = this.ctx.getImageData(0, 0, this.node.width, this.node.height);
		}
		return this._imageData;
	}

	difference(otherCanvas) {
		let data = this.getImageData();
		let dataOther = otherCanvas.getImageData();

		return util.difference(data, dataOther);
	}

	distance(otherCanvas) {
		let difference = this.difference(otherCanvas);
		return util.differenceToDistance(difference, this.node.width*this.node.height);
	}

	drawStep(step) {
		this.ctx.globalAlpha = step.alpha;
		this.ctx.fillStyle = step.color;
		step.shape.render(this.ctx);
		return this;
	}
}


module.exports = Canvas;