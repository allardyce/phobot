const fs = require('fs')
const path = require('path');
const Canvas = require('./canvas.js')
const Optimizer = require('./optimizer.js')
const Shape = require('./shape.js')

const load = require('../../helpers/load-files');
const mkdir = require('../../helpers/mkdir');

const XMLSerializer = require('xmlserializer');

const shapeMap = {
	"triangle": Shape.Triangle,
	"rectangle": Shape.Rectangle,
	"ellipse": Shape.Ellipse,
	"smiley": Shape.Smiley
}

const default_config = {
    computeSize: 256,
    viewSize: 512, 
    steps: 100, 
    shapes: 200, 
    alpha: 0.5,
    mutations: 30,
    mutateAlpha: true,
    shapeTypes: [ shapeMap['triangle'] ],
    fill: "rgb(153, 158, 159)",
    width:192,
    height:256,
    scale: 2
}

let steps;

function go(original, cfg) {

    return new Promise(resolve => {

        let optimizer = new Optimizer(original, cfg);
        steps = 0;

        let cfg2 = Object.assign({}, cfg, {width:cfg.scale*cfg.width, height:cfg.scale*cfg.height});
        let result = Canvas.empty(cfg2, false);
        result.ctx.scale(cfg.scale, cfg.scale);

        let svg = Canvas.empty(cfg, true);
        svg.setAttribute("width", cfg2.width);
        svg.setAttribute("height", cfg2.height);


        optimizer.onStep = (step) => {
            if (step) {
                result.drawStep(step);
                svg.appendChild(step.toSVG());
                let percent = (100*(1-step.distance)).toFixed(2);
                //console.log(`(${++steps} of ${cfg.steps}, ${percent}% similar)`);
                //console.log(XMLSerializer.serializeToString(svg));
            }
        }
        optimizer.start();
        
        optimizer.onFinish = (step) => {
            resolve(XMLSerializer.serializeToString(svg))
        }
    })

}

function optimize(image, config) {
    
    return new Promise(resolve => {

        config = Object.assign(default_config, config || {})
        
        fs.readFile(image, (err, data) => {

            Canvas.original(data, config)
                .then(original => {
                    return go(original, config)
                })
                .then(svg => {
                    resolve(svg)
                })

        })

    })
    
}


const svg = ( files, { steps } ) => {

    if (!steps) steps = 100;

    // let images = load(files);
    let images = files;

    console.log(`Found ${images.length} images.`);

    console.log(images);

    images.forEach(image => {

        optimize(image, { steps: steps })
            .then(svg => {
                
                let file = path.parse(image);

                fs.writeFileSync(path.join(file.dir, file.name + '.svg'), svg);

            })

        

    });

}

module.exports = svg;