const fs = require('fs');
const path = require('path');

const log = require('../helpers/log');
const parseInputOutput = require('../helpers/parse-input-output');

// Sort files into folders by thei extensions

const extensions = async (input, output, { recursive }) => {
    let files = await parseInputOutput(input, output, recursive, false);

    log(`Found ${files.length} files. Starting the sorting algorithms. Beep boop.`);

    files.forEach(file => {
        let parse = path.parse(file.input);

        let folder = parse.ext.toLowerCase().replace('.', '');

        if (!fs.existsSync(folder)) fs.mkdirSync(folder);

        let output = path.join(folder, `${parse.name}${parse.ext}`);

        log(`Moving ${file.input} to ${output}`);

        fs.renameSync(file.input, output);
    });
};

module.exports = extensions;
