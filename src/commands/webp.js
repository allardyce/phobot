const fs = require('fs');
const { promisify } = require('util');
const readFile = promisify(fs.readFile);
const imagemin = require('imagemin');
const imageminWebp = require('imagemin-webp')

const parseInputOutput = require('../helpers/parse-input-output');
const formatBytes = require('../helpers/format-bytes');
const log = require('../helpers/log');


const compressImage = async (imageBuffer, options) => {
    return await imagemin.buffer(imageBuffer, {
        use: [
            imageminWebp({
                quality: 70
            })
        ]
    });
}


const compress = async (input, output, { recursive, quality }) => {

    let files = await parseInputOutput(input, output, recursive);

    log(`Found ${files.length} files. Starting compression algorithms. Beep boop.`);

    files.forEach(async file => {

        let original = await readFile(file.input);

        let compressed = await compressImage(original, { quality });

        if (compressed.length > original.length) {
            log(`Compression increased filesize, not saving. ${file.input} (${formatBytes(original.length)}) -> ${file.output} (${formatBytes(compressed.length)}) - (${(compressed.length / original.length * 100).toFixed(2)}%)`);
            return;
        }

        let output = file.output.split('.');
        output.pop();
        output.push('webp');
        file.output = output.join('.');

        let writeStream = fs.createWriteStream(file.output);

        writeStream.write(compressed);

        writeStream.on('finish', () => {
            log(`Saved ${file.input} (${formatBytes(original.length)}) -> ${file.output} (${formatBytes(compressed.length)}) - (${(compressed.length / original.length * 100).toFixed(2)}%)`);
        });
        
        writeStream.end();

    });

}

module.exports = compress;