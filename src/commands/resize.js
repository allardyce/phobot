const fs = require('fs');
const sharp = require('sharp');

const log = require('../helpers/log');
const formatBytes = require('../helpers/format-bytes');
const parseInputOutput = require('../helpers/parse-input-output');

const resize = async (input, output, { recursive, width = 2048, height = 2048, fit = 'inside', withoutEnlargement }) => {
    if (!width && !height) {
        log.error('Missing width or height');
        process.exit(1);
    }

    let files = await parseInputOutput(input, output, recursive);

    log(`Found ${files.length} files. Starting resize algorithms. Beep boop.`);

    files.forEach(file => {
        let fileStats = fs.statSync(file.input);

        sharp(file.input, { failOnError: false })
            .resize({
                width: width,
                height: height,
                fit: fit,
                withoutEnlargement
            })
            .toBuffer()
            .then(resized => {
                let writeStream = fs.createWriteStream(file.output);

                writeStream.write(resized);

                writeStream.on('finish', () => {
                    log(`Resized ${file.input} (${formatBytes(fileStats.size)}) -> ${file.output} (${formatBytes(resized.length)}) - (${((resized.length / fileStats.size) * 100).toFixed(2)}%)`);
                });

                writeStream.end();
            });
    });
};

module.exports = resize;
