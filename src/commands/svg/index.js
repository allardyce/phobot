const fs = require('fs');
const Vectato = require('@allardyce/vectato').default;

const log = require('../../helpers/log');
const formatBytes = require('../../helpers/format-bytes');
const parseInputOutput = require('../../helpers/parse-input-output');

const svgo = require('./svgo');

const resize = async (input, output, { recursive, steps = 100, mutations = 10 }) => {

    let files = await parseInputOutput(input, output, recursive);

    log(`Found ${files.length} files. Starting resize algorithms. Beep boop.`);

    files.forEach(async file => {

        let output = file.output.split('.');
        output.pop();
        output.push('svg');
        file.output = output.join('.');

        let fileStats = fs.statSync(file.input);

        let svg = await Vectato.trace(file.input, { steps: steps, mutations: mutations });

        console.log(svg);

        svg = (await svgo.optimize(svg)).data;

        console.log(svg);

        fs.writeFileSync(file.output, svg);

        log(`Vectatoed ${file.input} (${formatBytes(fileStats.size)}) -> ${file.output} (${formatBytes(svg.length)}) - (${(svg.length / fileStats.size * 100).toFixed(2)}%)`);

    })

}


module.exports = resize;
