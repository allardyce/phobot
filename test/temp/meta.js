module.exports = [
{
    large: { src: '/assets/gen/IMG_1138.2048.jpg', w: 2048, h: 1365 },
    medium: { src: '/assets/gen/IMG_1138.1024.jpg', w: 1024, h: 683 },
    msrc: '/assets/gen/IMG_1138.384.jpg',
    date: 1529072755,
    background: '#8f979e',
},
{
    large: { src: '/assets/gen/IMG_1140.2048.JPG', w: 2048, h: 1365 },
    medium: { src: '/assets/gen/IMG_1140.1024.JPG', w: 1024, h: 683 },
    msrc: '/assets/gen/IMG_1140.384.JPG',
    date: 1529077684,
    background: '#7f7c7b',
},
{
    large: { src: '/assets/gen/20180723_113224.2048.jpg', w: 2048, h: 561 },
    medium: { src: '/assets/gen/20180723_113224.1024.jpg', w: 1024, h: 281 },
    msrc: '/assets/gen/20180723_113224.384.jpg',
    date: 1532345544,
    background: '#576d85',
    gps: { lat: 50.05888888888889, lng: -122.96361111111112 }
},
]