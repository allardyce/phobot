module.exports = [
{
    large: { src: '/assets/gen/road-trip/15/IMG_1130.2048.JPG', w: 2048, h: 1365 },
    medium: { src: '/assets/gen/road-trip/15/IMG_1130.1024.JPG', w: 1024, h: 683 },
    msrc: '/assets/gen/road-trip/15/IMG_1130.384.JPG',
    date: 1529004871,
    background: '#7b7e78',
},
{
    large: { src: '/assets/gen/road-trip/15/20180615_103549.2048.jpg', w: 2048, h: 1152 },
    medium: { src: '/assets/gen/road-trip/15/20180615_103549.1024.jpg', w: 1024, h: 576 },
    msrc: '/assets/gen/road-trip/15/20180615_103549.384.jpg',
    date: 1529058948,
    background: '#787e85',
    gps: { lat: 51.500277777777775, lng: -117.18472222222222 }
},
{
    large: { src: '/assets/gen/road-trip/15/IMG_1135.2048.JPG', w: 2048, h: 1365 },
    medium: { src: '/assets/gen/road-trip/15/IMG_1135.1024.JPG', w: 1024, h: 683 },
    msrc: '/assets/gen/road-trip/15/IMG_1135.384.JPG',
    date: 1529072457,
    background: '#6a5944',
},
{
    large: { src: '/assets/gen/road-trip/15/20180616_102816.2048.jpg', w: 2048, h: 1152 },
    medium: { src: '/assets/gen/road-trip/15/20180616_102816.1024.jpg', w: 1024, h: 576 },
    msrc: '/assets/gen/road-trip/15/20180616_102816.384.jpg',
    date: 1529144895,
    background: '#595b52',
    gps: { lat: 51.50083333333333, lng: -117.18472222222222 }
},
]