module.exports = [
{
    large: { src: '/assets/gen/road-trip/16/20180618_183009.2048.jpg', w: 1152, h: 2048 },
    medium: { src: '/assets/gen/road-trip/16/20180618_183009.1024.jpg', w: 576, h: 1024 },
    msrc: '/assets/gen/road-trip/16/20180618_183009.384.jpg',
    date: 1529346609,
    background: '#b6a8a7',
    gps: { lat: 50.99888888888889, lng: -118.1975 }
},
{
    large: { src: '/assets/gen/road-trip/16/20180621_084622.2048.jpg', w: 2048, h: 1152 },
    medium: { src: '/assets/gen/road-trip/16/20180621_084622.1024.jpg', w: 1024, h: 576 },
    msrc: '/assets/gen/road-trip/16/20180621_084622.384.jpg',
    date: 1529570782,
    background: '#7d7e7f',
    gps: { lat: 50.85805555555556, lng: -118.55694444444444 }
},
{
    large: { src: '/assets/gen/road-trip/16/20180621_091537.2048.jpg', w: 2048, h: 1152 },
    medium: { src: '/assets/gen/road-trip/16/20180621_091537.1024.jpg', w: 1024, h: 576 },
    msrc: '/assets/gen/road-trip/16/20180621_091537.384.jpg',
    date: 1529572537,
    background: '#6f6d5b',
    gps: { lat: 50.85805555555556, lng: -118.55694444444444 }
},
{
    large: { src: '/assets/gen/road-trip/16/20180621_091603.2048.jpg', w: 1152, h: 2048 },
    medium: { src: '/assets/gen/road-trip/16/20180621_091603.1024.jpg', w: 576, h: 1024 },
    msrc: '/assets/gen/road-trip/16/20180621_091603.384.jpg',
    date: 1529572563,
    background: '#7d7e79',
    gps: { lat: 50.85805555555556, lng: -118.55666666666666 }
},
{
    large: { src: '/assets/gen/road-trip/16/20180621_145927.2048.jpg', w: 2048, h: 1152 },
    medium: { src: '/assets/gen/road-trip/16/20180621_145927.1024.jpg', w: 1024, h: 576 },
    msrc: '/assets/gen/road-trip/16/20180621_145927.384.jpg',
    date: 1529593167,
    background: '#5a6c3b',
    gps: { lat: 50.861111111111114, lng: -118.39500000000001 }
},
]